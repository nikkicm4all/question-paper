var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var postSchema = new mongoose.Schema({
	title: String,
	quests:[
		{
			quest: String,
			a:String,
			b:String,
			c:String,
			d:String,
			key:String

		}
	]
});
var userSchema = new mongoose.Schema({
	username: String,
	password: String
});

mongoose.model('Post', postSchema);
mongoose.model('User', userSchema);
