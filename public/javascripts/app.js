var app = angular.module('chatApp', ['ngResource','ngMaterial','ui.router']).run(function($http, $rootScope) {
	$rootScope.authenticated = false;
	$rootScope.current_user = 'Guest';
	$rootScope._id = null;

	$rootScope.signout = function(){
		$http.get('auth/signout');
		$rootScope.authenticated = false;
		$rootScope.current_user = 'Guest';
        $location.path('/login');
    };
});
/*app.config(function($routeProvider){
	$routeProvider
	//the timeline display
		.when('/', {
			templateUrl: 'main.html',
			controller: 'mainController'
		})
		//the login display
		.when('/login', {
			templateUrl: 'login.html',
			controller: 'authController'
		})
		.when('/socket', {
			templateUrl: 'socket.io.html',
			controller: 'socketController'
		})
		//the signup display
		.when('/register', {
			templateUrl: 'register.html',
			controller: 'authController'
		})
	.when('/paper/:id', {
		templateUrl: 'paper.html',
		controller: 'paperController'
	})
});*/
app.config(function($stateProvider) {
	$stateProvider

		.state('home', {
			url: '',
			templateUrl: 'view.html',
			controller: 'mainController'
		})
        .state('qhome', {
            url: '/view',
            templateUrl: 'view.html',
            controller: 'mainController'
        })

		.state('paper', {
			url: '/paper/{id}',
			templateUrl: 'paper.html',
			controller: 'paperController'
		})
		.state('create', {
			url: '/create',
			templateUrl: 'create.html',
			controller: 'mainController'
		})
		.state('register', {

			url: '/register',
			templateUrl: 'register.html',
			controller: 'authController'

		})
		.state('login', {

			url: '/login',
			templateUrl: 'login.html',
			controller: 'authController'

		});
});
/*
 //used for basic read from json
 app.factory('postService', function($http){
 var baseUrl = "/api/posts";
 var factory = {};
 factory.getAll = function(){
 return $http.get(baseUrl);
 };
 return factory;
 });
 */


app.controller('AppCtrl',function($scope, $http, $rootScope, $location) {
    $scope.currentNavItem = 'page1';

});

app.controller('toast', function($scope, $mdToast) {
    var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };

    $scope.toastPosition = angular.extend({},last);

    $scope.getToastPosition = function() {
        sanitizePosition();

        return "bottom left";
        /*Object.keys($scope.toastPosition)
            .filter(function(pos) { return $scope.toastPosition[pos]; })
            .join(' ');*/
    };

    function sanitizePosition() {
        var current = $scope.toastPosition;

        if ( current.bottom && last.top ) current.top = false;
        if ( current.top && last.bottom ) current.bottom = false;
        if ( current.right && last.left ) current.left = false;
        if ( current.left && last.right ) current.right = false;

        last = angular.extend({},current);
    }

    $scope.showSimpleToast = function() {
        var pinTo = $scope.getToastPosition();

        $mdToast.show(
            $mdToast.simple()
                .textContent('Question Added!')
                .position(pinTo )
                .hideDelay(3000)
        );
    };
    
})

    .controller('ToastCtrl', function($scope, $mdToast) {
        $scope.closeToast = function() {
            $mdToast.hide();
        };
    });
app.controller('paperController', function($scope, $rootScope, postService,$stateParams){
	console.log($stateParams.id );

	$scope.quest = postService.get({ id: $stateParams.id }, function() {
		$scope.len = $scope.quest.quests.length;
	});

	$scope.mar = function() {
		$scope.cor=0;
		$scope.inc=0;
		$scope.no=0;
		$scope.mark=0;
		for(var i=0;i<$scope.quest.quests.length;i++)
		{
			if($scope.quest.quests[i].ans == $scope.quest.quests[i].key )
			{
				$scope.cor=$scope.cor+1;
				$scope.mark=$scope.mark+5;
			}
			else if($scope.quest.quests[i].ans != null)
			{
				$scope.inc=$scope.inc+1;
				$scope.mark=$scope.mark-2;
			}
			else
			{
				$scope.no=$scope.no+1;

			}
		}


	};

	/*$scope.quest = postService.get({});
	console.log($scope.quest);*/

});

app.service('Socket',
	function () {
		// Connect to Socket.io server
		this.connect = function () {
			// Connect only when authenticated

			this.socket = io();

		};
		this.connect();

		// Wrap the Socket.io 'on' method
		this.on = function (eventName, callback) {
			if (this.socket) {
				this.socket.on(eventName, function (data) {

						callback(data);

				});
			}
		};

		// Wrap the Socket.io 'emit' method
		this.emit = function (eventName, data) {
			if (this.socket) {
				this.socket.emit(eventName, data);
					console.log('working');
			}


		};

		// Wrap the Socket.io 'removeListener' method
		this.removeListener = function (eventName) {
			if (this.socket) {
				this.socket.removeListener(eventName);
			}
		};
	}
);


app.factory('postService', function($resource){
	return $resource('/api/quest/:id');
});

app.controller('mainController', function($scope, $rootScope, postService){
	$scope.quest = postService.query();
	$scope.newPost = {title: '',createdby:''};
	$scope.newPost.quests=[];

	$scope.newPost.quests.push({dummy:''});
	$scope.post = function() {
		postService.save($scope.newPost, function(){
			$scope.quest = postService.query();

		});
	};

	$scope.add = function() {
		$scope.newPost.quests.push({dummy:''});

	};
	$scope.delete = function(post)	{
		postService.delete({id: post._id});
		$scope.quest = postService.query();
	};
});
app.controller('authController', function($scope, $http, $rootScope, $location){
	$scope.user = {username: '', password: ''};
	$scope.error_message = '';

	$scope.login = function(){
		$http.post('/auth/login', $scope.user).success(function(data){
			if(data.state == 'success'){
				$rootScope.authenticated = true;
				$rootScope.current_user = data.user.username;
				$rootScope._id = data.user._id;
				$location.path('/view');
			}
			else{
				$scope.error_message = data.message;
			}
		});
	};
	

	$scope.register = function(){
		$http.post('/auth/signup', $scope.user).success(function(data){
			if(data.state == 'success'){
				$rootScope.authenticated = true;
				$rootScope.current_user = data.user.username;
				$rootScope._id = data.user._id;
				$location.path('/view');
			}
			else{
				$scope.error_message = data.message;
			}
		});
	};
});

app.controller('navController', function($scope, $http, $rootScope, $location){
	
});

app.controller('socketController', function($scope, $http, $rootScope, $location ){

	

	$scope.messages = [];
	$scope.messageText = '';
	$scope.sendMessage = sendMessage;
	var socket = io();
	function sendMessage() {
		var msg={text:$scope.messageText,createdby: $rootScope.current_user};
		socket.emit('chat message', msg);
		$scope.messageText='';
	}




	socket.on('chat message', function(msg){
		$scope.messages.push(msg);
	});

	

		
});